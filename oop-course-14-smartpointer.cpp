#include <iostream>
#include <vector>
#include <algorithm>

// Accumulator
template<class T> class SumMe
{
public:
     SumMe(T i=0) : sum{i}
     {
     }

     void operator() (T x)
     {
         //std::cout << "Adding " << x << std::endl;
         sum += x;
         //std::cout << "Sum = " << sum << std::endl;
     }

     T result() const
     {
         return sum;
     }
private:
     T sum;
};

// SmartPointer class is covariant
template <class T> class SmartPointer
{
public:
    template <typename U>
    SmartPointer(U* p) : p_{p} {}

    template <typename U>
    SmartPointer(const SmartPointer<U>& sp,
                 typename std::enable_if<std::is_convertible<U*, T*>::value, void>::type * = 0)
      : p_{sp.p_} {}

    template <typename U>
    typename std::enable_if<std::is_convertible<U*, T*>::value, SmartPointer<T>&>::type
    operator=(const SmartPointer<U> & sp)
    {
        p_ = sp.p_;
        return *this;
    }

   T* p_;
};

// Tests
void test_accumulator()
{
    std::vector<int> v;

    for(int i=0; i<32; i++)
          v.push_back(i);

    // invoke acc(v[0]), acc(v[1]), ..., sum(v[31])
    SumMe<int> acc = std::for_each(v.begin(), v.end(), SumMe<int>{});

    std::cout << "Total = " << acc.result() << std::endl;
}

struct Vehicle
{
};

struct Car : public Vehicle
{
};

void test_upcast()
{
    SmartPointer<Car> pc = new Car;
    SmartPointer<Vehicle> pv = pc; // ctor

    pv = pc; // operator=
}

void test_downcast()
{
    SmartPointer<Vehicle> pv = new Vehicle;
    //error: conversion from 'SmartPointer<Vehicle>' to non-scalar type 'SmartPointer<Car>' requested
    //SmartPointer<Car> pc = pv; // ctor

    //pc = pv; // operator=
}


int main()
{
    test_accumulator();

    test_upcast();

    test_downcast();

    return 0;
}
