#include <iostream>

using namespace std;

class Date 
{
    int _day, _month, _year;

public:
    // Constructors!
    Date(int day = 0, int month = 0, int year = 0);
     
    Date(const Date& rvalue);
     
    void print() const;
     
    int& day();
    Date& day(int day);
};

Date defaultDate{1, 1, 2000};

Date::Date(int day, int month, int year) 
{
    cout << "Date::Date(" << day << ", " << month << ", " << year << ")" << endl;
    _day = day ? day : defaultDate._day;
    _month = month ? month : defaultDate._month;
    _year = year ? year : defaultDate._year;
}

Date::Date(const Date& rvalue)
{
    cout << "Date::Date("; rvalue.print(); cout << ")" << endl;
    _day = rvalue._day;
    _month = rvalue._month;
    _year = rvalue._year;
}

void Date::print() const
{
    cout << _day << "/" << _month << "/" << _year << endl;
}

int& Date::day()
{
    return _day;
}

Date& Date::day(int day)
{
    _day = day;
    return *this;
}

void test_When_OldSyntax_Then_ConstructorsAreInvoked()
{
    Date d0 = Date(6, 10, 2003);
    d0.print();
    Date d1(1, 1);
    d1.print();
    Date d2(5);
    d2.print();
    Date d3; 
    d3.print();
    Date* pd = new Date(7, 10, 2003);
    pd->print();
}
    
void test_When_NewSyntax_Then_ConstructorsAreInvoked()
{
    Date d0 = Date{6, 10, 2003};
    d0.print();
    Date d1{1, 1};
    d1.print();
    Date d2{5};
    d2.print();
    Date d3; 
    d3.print();
    Date* pd = new Date{7, 10, 2003};
    pd->print();
}
   
void test_When_CopyCtorInvoked_Then_CloneRValue()
{
    Date d{13, 4, 2021};
    cout << "d = "; d.print();
    
    Date clone = d;
    cout << "clone = "; clone.print();
    
    d.day() = 14;
    // d.day(14);
    
    cout << "After source change" << endl;
    cout << "d = "; d.print();
    cout << "clone = "; clone.print();
}

void f(Date d)
{
    cout << "In f(): "; d.print();
}

void test_When_Argument_Then_CopyCtor()
{
    Date d{13, 4, 2021};
    
    f(d);
}

void g(Date& d)
{
    cout << "In g(): "; d.print();
    d.day() = 15;
    d.print();
} 

void test_When_Argument_Then_CopyCtor2()
{
    Date d{13, 4, 2021};
    
    g(d);
    
    d.print();
}

int main()
{
    //test_When_OldSyntax_Then_ConstructorsAreInvoked();
    
    //test_When_NewSyntax_Then_ConstructorsAreInvoked();
    
    // test_When_CopyCtorInvoked_Then_CloneRValue();
    
    // test_When_Argument_Then_CopyCtor();
    
    test_When_Argument_Then_CopyCtor2();
    
    return 0;
}