#include <iostream>
#include "Date.h"

using namespace std;

class Employee
{
public:
    Employee(const string n, const Date& d);
    void print() const;
    virtual void vprint() const;

private:
     string name;
     Date dob; // birth date
     Date doh; // hiring date
};

Employee::Employee(const string n, const Date& d)
    : name{n}, dob{d}
{
    cout << "Employee::Employee(" << name << ", " << dob << ")" << endl;
}

void Employee::print() const
{
    cout << "Employee(name: " << name << ", dob: "<< dob << ", doh: " << doh << ")" << endl;
}

void Employee::vprint() const
{
    print();
}

struct EmployeeList
{
    void add(Employee* pe);
};

void EmployeeList::add(Employee* pe)
{
    // No operation implementation
}

class Manager : public Employee
{
public:
    Manager(const char* name, const Date& dob);
    int level;

    void print() const;
    void vprint() const override;

private:
    // list of managed persons
    EmployeeList managedGroup;
};

Manager::Manager(const char* n, const Date& d)
    : Employee{n, d}
{
    cout << "Manager::Manager(" << n << ")" << endl;
    // error: ‘std::string Employee::name’ is private within this context
    // cout << "Manager::Manager(" << name << ")" << endl;
}

void Manager::print() const
{
    Employee::print(); // call base class member
    cout << "Manager(group: " << &managedGroup << ", level: " << level << ")" << endl;
}

void Manager::vprint() const
{
    print();
}

// interface
struct Factory
{
    virtual Employee* create(const char* name, const Date& dob) = 0;
};

struct DerivedFactory : Factory
{
    Manager* create(const char* name, const Date& dob) override
    {
        return nullptr; // new Manager(name, dob);
    }
};


// ------ Tests ------
//

void test_When_Derived_Instatiated_Then_Base_Instatiated_Too()
{
    Date d{28, 10, 1955};
    Manager m{"Bill Gates", d};
}

void test_UpCasting()
{
    Manager alex{"Alex Ferguson", Date{31, 12, 1941}};

    Employee* pe = &alex;

    Employee& re = alex;

    EmployeeList l;

    l.add(new Employee{"John Doe", Date{3, 4, 2010}});
    l.add(&alex);
}

void test_DownCasting()
{
    Employee john{"John Doe", Date{3, 4, 2010}};

    // error: invalid conversion from ‘Employee*’ to ‘Manager*’
    // Manager* pm = &john;

    // error: invalid initialization of reference of type ‘Manager&’ from expression of type ‘Employee’
    // Manager& rm = john;

    // Casting is enforced; no checks are performed
    // Manager* pm2 = static_cast<Manager*>(&john);

    // pm2->print();

    // Random result!
    // cout << pm2->level;

    // error: cannot dynamic_cast ‘& john’ (of type ‘class Employee*’) to type ‘class Manager*’ (source type is not polymorphic)
    // Manager* pm3 = dynamic_cast<Manager*>(&john);
}

void test_When_Slicing()
{
    Manager m{"Bill Gates", Date{28, 10, 1955}};

    Employee e = m;

    e.print();
}

void test_When_Same_Prototype_Then_Call_Based_On_Type()
{
    Manager m{"Bill Gates", Date{28, 10, 1955}};

    // Manager::print
    cout << "1: ";
    m.print();

    // Manager::print
    cout << "2: ";
    ((Manager *)&m)->print();

    // Employee::print
    cout << "3: ";
        ((Employee *)&m)->print();
}

void test_When_Virtual_Function_Then_Calls_Overriden_Function()
{
    Manager m{"Bill Gates", Date{28, 10, 1955}};

    // Manager::vprint
    cout << "1: ";
    m.vprint();

    // Manager::vprint
    cout << "2: ";
    ((Manager *)&m)->vprint();

    // Manager::vprint
    cout << "3: ";
    ((Employee *)&m)->vprint();

    // Force Employee::vprint
    cout << "4: ";
    ((Employee *)&m)->Employee::vprint();

    // Employee::print
    cout << "5: ";
    ((Employee *)&m)->print();
}

void test_Covariance()
{
    DerivedFactory factory;

    Manager* p1 = factory.create("Alex Ferguson", Date{31, 12, 1941});

    Employee* p2 = ((Factory *)&factory)->create("Bill Gates", Date{28, 10, 1955});

    p2->vprint();
}


int main()
{
    // test_When_Derived_Instatiated_Then_Base_Instatiated_Too();

    // test_UpCasting();

    // test_DownCasting();

    // test_When_Slicing();

    // test_When_Same_Prototype_Then_Call_Based_On_Type();

    // test_When_Virtual_Function_Then_Calls_Overriden_Function();

    test_Covariance();
}
