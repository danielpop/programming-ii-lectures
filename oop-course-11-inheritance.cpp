#include <iostream>
#include "Date.h"

using namespace std;

class Employee 
{
public:
    Employee(const string n, const Date& d);
    void print() const;

private:
    string name;
    Date dob; // birth date
    Date doh; // hiring date
}; 

Employee::Employee(const string n, const Date& d)
    : name{n}, dob{d}
{
    cout << "Employee::Employee(" << name << ", " << dob << ")" << endl;
}

void Employee::print() const
{
    cout << "Employee(name: " << name << ", dob: "<< dob << ", doh: " << doh << ")" << endl;
}
    
struct EmployeeList 
{
    void add(Employee* pe);
};

void EmployeeList::add(Employee* pe)
{
    // No operation implementation
}

class Manager : public Employee 
{
public:     
    Manager(const char* n, const Date& d);
    int level;

private:
    // list of managed persons
    EmployeeList managedGroup;  
};

Manager::Manager(const char* n, const Date& d)
    : Employee{n, d}
{
    cout << "Manager::Manager(" << n << ")" << endl;
    // error: ‘std::string Employee::name’ is private within this context
    // cout << "Manager::Manager(" << name << ")" << endl;
}

void test_When_Derived_Instatiated_Then_Base_Instatiated_Too()
{
    Date d{28, 10, 1955};
    Manager m{"Bill Gates", d};
    m.print();
    // error: ‘std::string Employee::name’ is private within this context
    // cout << m.name;
}

void test_UpCasting()
{
    Manager alex{"Alex Ferguson", Date{31, 12, 1941}};
    
    Manager* pm = &alex;
    
    Employee* pe = &alex;
    pe->print();
    
    Employee& re = alex;
    
    EmployeeList l;
    
    l.add(new Employee{"John Doe", Date{3, 4, 2010}});
    l.add(&alex);
}

void test_DownCasting()
{
    Employee john{"John Doe", Date{3, 4, 2010}};
    
    // error: invalid conversion from ‘Employee*’ to ‘Manager*’
    // Manager* pm = &john;
    
    // error: invalid initialization of reference of type ‘Manager&’ from expression of type ‘Employee’
    // Manager& rm = john;
    
    // Casting is enforced; no checks are performed
    // Manager* pm2 = static_cast<Manager*>(&john);
    
    // pm2->print();
    
    // Random result!
    // cout << pm2->level;
    
    // error: cannot dynamic_cast ‘& john’ (of type ‘class Employee*’) to type ‘class Manager*’ (source type is not polymorphic)
    // Manager* pm3 = dynamic_cast<Manager*>(&john);
}

void test_When_Slicing()
{
    Manager m{"Bill Gates", Date{28, 10, 1955}};
 
    // because copy ctor Employee(const Employee&), 
    // we can init an Employee given a Manager instance
    Employee e = m;
    
    e.print();
}

int main()
{
    // test_When_Derived_Instatiated_Then_Base_Instatiated_Too();
    
    // test_UpCasting();
    
    // test_DownCasting();
    
    test_When_Slicing();
}
