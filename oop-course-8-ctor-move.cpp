#include <iostream>
#include <string.h>
#include <stdio.h>

using namespace std;

struct A 
{
    string s;
    
    // default ctor
    A() //: s{"test"} 
    { 
        s = "test";
        std::cout << "default ctor" << std::endl;
    }
    
    // copy ctor
    A(const A& o) //: s(o.s)
    { 
        s = o.s;
	    std::cout << "copy ctor" << std::endl; 
    }   
    
    // move ctor
    A(const A&& o) noexcept //: s(std::move(o.s)) 
    { 
        s = std::move(o.s);
        std::cout << "move ctor" << std::endl;
    }
}; 

A f(A a) 
{    
    return a; // will call move constructor
} 

void test_WhenMoveIsInvoked_Then_MoveCtorIsInvoked()
{
    A a1;
    A a2 = std::move(a1);
    
    std::cout << "a1.s = " << a1.s << std::endl;
    std::cout << "a2.s = " << a2.s << std::endl;
}

void test_WhenInitialization_Then_CopyCtorIsInvoked()
{
    A a1;
    A a2 = a1;
    
    std::cout << "a1.s = " << a1.s << std::endl;
    std::cout << "a2.s = " << a2.s << std::endl;
}

void test_WhenReturnedArgument_Then_MoveCtorIsInvoked()
{
    A a1;
    A a2 = f(a1);
    
    std::cout << "a1.s = " << a1.s << std::endl;
    std::cout << "a2.s = " << a2.s << std::endl;
}

int main()
{
    // test_WhenInitialization_Then_CopyCtorIsInvoked();
    
    // test_WhenMoveIsInvoked_Then_MoveCtorIsInvoked();
    
    // test_WhenReturnedArgument_Then_MoveCtorIsInvoked();
    
    return 0;
}