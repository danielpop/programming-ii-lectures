#include <iostream>

using namespace std;

void f(int i, int& ri, int* pi)
{
    i = 10;
    ri = 10;
    *pi = 10;
}

void test_BasicReference()
{
    int x = 7, y = 8, z = 9;
    
    int& ri0 = x;      // OK
    // int& ri1 = 7;      // ERROR: not possible
    const int& ri2 = 7; // OK
    
    // int& ria[10];      // ERROR: array of references
    // int& *pri = &x;    // ERROR: pointer to reference
    
    f(x, y, &z);
    cout << "x = " << x << ", y = " << y << ", z = " << z << endl;    
}

char at0(char* s, int n) 
{
    return s[n];
}

char& at1(char* s, int n) 
{
    return s[n];
}

void test_ReferenceAsReturnValue()
{
    char myName[] = "Daniel Pop";
    
    cout << myName << endl;
    
    cout << "myName[7] = " << at0(myName, 7);
    cout << ", myName[7] = " << at1(myName, 7) << endl;
    
    // at0(myName, 7) = 'D'; // ERROR: not possible to be used as lvalue
    // cout << "myName[7] after at0 = " << myName[7] << endl;
    
    at1(myName, 7) = 'D';
    cout << "myName[7] after at1 = " << myName[7] << endl;
    cout << myName;    
}

int main()
{
    // test_BasicReference();
    
    // test_ReferenceAsReturnValue();
    
    return 0;
}
