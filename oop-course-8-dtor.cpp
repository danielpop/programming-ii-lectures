#include <iostream>

using namespace std;

class Date 
{
    int _day, _month, _year;

public:
    // Constructors!
    Date(int day = 0, int month = 0, int year = 0);
     
    // Copy ctor
    Date(const Date& rvalue);
	
	// Destructor
	~Date();
    
    // Type conversion ctor: const char* => Date
    /*explicit*/ Date(const char* rep);
    
    // Accessor member functions 
    int& day();
    
    // Operations
    void print() const;
};

Date defaultDate{1, 1, 2000};

Date::Date(int day, int month, int year) 
{
    cout << "Date::Date(" << day << ", " << month << ", " << year << ") => " << hex << this << dec << endl;
    _day = day ? day : defaultDate._day;
    _month = month ? month : defaultDate._month;
    _year = year ? year : defaultDate._year;
}

Date::Date(const Date& rvalue)
{
    cout << "Date::Date(" << _day << "/" << _month << "/" << _year << ") => " << hex << this << dec << endl;
    _day = rvalue._day;
    _month = rvalue._month;
    _year = rvalue._year;
}

Date::Date(const char* rep)
{
    cout << "Date::Date(\"" << rep << "\") => " << hex << this << dec << endl;
    // TODO: parse the rep and fill in _day, _month, _year
    // Just set to some values
    _day = 1;
    _month = 1;
    _year = 2010;
}

Date::~Date()
{
	cout << "Date::~Date " << hex << this << dec << " "; print();
}

void Date::print() const
{
    cout << _day << "/" << _month << "/" << _year << endl;
}

int& Date::day()
{
    return _day;
}


void processDate(Date d)
{
    static Date staticDate{1, 3, 2021};
    
    cout << "Processing the following date: ";
    d.print();
}

void test_WhenObjectGoesOutOfScope_Then_DestructorGetsInvoked()
{
	Date local1{2, 2, 2021};
	Date local2{3, 2, 2021};

	if (local1.day() > 1)
	{
		Date copyLocal = local1;
		copyLocal.print();
	}
}

void test_WhenDynamicallyAllocated_Then_DestructorGetsInvokedWhenDeleteIsCalled()
{
	Date *p1 = new Date{1, 2, 2021};
	Date *p2 = new Date{2, 2, 2021};

    // As we call delete for p1 only, the dtor for p2 doesn't get invoked
	delete p1;
}

void test_WhenStaticObjectGoesOutOfScope_Then_DestructorGetsInvoked()
{
	Date local1{2, 2, 2021};
	Date local2{3, 2, 2021};

	processDate(local2);
	
	processDate(local1);
}

int main()
{
	// test_WhenObjectGoesOutOfScope_Then_DestructorGetsInvoked();
	
 	// test_WhenDynamicallyAllocated_Then_DestructorGetsInvokedWhenDeleteIsCalled();
	
 	// test_WhenStaticObjectGoesOutOfScope_Then_DestructorGetsInvoked();
    
    return 0;
}

